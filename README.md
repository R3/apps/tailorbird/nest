## Tailorbird

Tailorbird aims at providing the handiest and most straight-forward experience when generating a static website.

## How to get started

- First of all, be sure to have Docker installed on your device: https://www.docker.com 

- After Docker is installed on the device, clone/fork the Tailorbird repository

- Make sure the config.json file corresponds to your needs

- Run the Tailorbird.py script

## The tools

Tailorbird offers a great variety of tools to select from when executing it.

1. Registrator:     https://git-r3lab.uni.lu/R3/apps/registrator
2. Linkchecker:     https://git-r3lab.uni.lu/R3/apps/linkchecker
3. Spellchecker:    https://git-r3lab.uni.lu/R3/apps/spellchecker

## The configuration process

The config.json file is where all the specifications are set, such as which 
tools are used and where they are pulled from. 

Every tool has two shared parameters, `active` and `url`.
- `active` is either ON (`1`) or OFF (`0`). 
- `url` is the link to the registry in which the tool's docker image is pulled from.

The tools may be switched on or off according to the needs, but the url should ideally not be changed from the default value

More complex tools like the spellchecker or the linkchecker, which require files to be fed to it, have more options at their disposition.

# Linkchecker

- `mount` is the location of the folder containing all the files that the linkchecker will go over. By default, it takes all the files present in the **linkCheck** folder in this repository.
- `whitelist` is a list of all the whitelisted url that the linkchecker will ignore when checking. Each entry in this list must follow the following convention:
`"someWhitelistedUrl"`, all element of the list is comma separated from the other within the already present bracket.

# Spellchecker

- `mount` is the location of the files to check and the dictionaries these files will be compared to. `dictionaries` holds the list of folders in which the dictionary files are stored. By default, it only points to the **dictionaries** folder in this repository. `files` is the location of the folder containing all the files that the spellchecker will go over. By default, it takes all the files present in the **spellCheck** folder in this repository.
- `aspell` generates the command for aspell to use. The conversion happens as follows:  
`"lang": ["en"]` becomes `--lang=en`  
If multiple elements are present in the list, an option is generated for each entry, as follows:  
`"lang": ["en", "fr"]` becomes `--lang=en --lang=fr`

NOTE: When adding flags and variables to `aspell`, please make sure the option is supported by aspell to avoid errors.
