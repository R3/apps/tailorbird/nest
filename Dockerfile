## -----------------------
## STAGE 1 : BASE IMAGE ##
## -----------------------
# take the official python image as base for this image
FROM python:3.5.9-buster AS tailorbird

# add the requirements specified
ADD requirements.txt .
RUN pip install -r requirements.txt

# add the main configuration file
ADD config.json .

# add the Tailorbird python script
ADD ./tailorbird .

CMD ["python3", "tailorbird.py"]