import docker
import sys
import os
import subprocess
import json

def command(cmd):
    # function that executes the docker command and prints it on the terminal.
    print ('>>> '+cmd)
    subprocess.call(cmd, shell=True)

def cleanText():
    # function that cleans out all non-requirement.txt text files.
    dir_name = os.getcwd()
    clear = os.listdir(dir_name)

    for item in clear:
        if item != 'requirements.txt':
            if item.endswith(".txt"):
                os.remove(os.path.join(dir_name, item))

def tailorbirdLinkchecker(url, pwd):
    # function that creates the docker command for the Linkchecker tool.
    # the command is written into a text file that is then mounted into the image.
    wlist = open('jsonWhitelist.txt', "w")

    for wl in data['tools']['linkchecker']['whitelist']:
        wlist.write(wl+"\n")
    wlist.close()

    whitelistDir = pwd+"/jsonWhitelist.txt"
    linkcheckDir = ""
    for lcMount in data['tools']['linkchecker']['mount']:
        lcDir = pwd+"/"+lcMount+":/checkFolder/ "
        linkcheckDir = linkcheckDir+lcDir
    
    cmd = "docker run "+whitelistDir+":/jsonWhitelist/jsonWhitelist.txt "+linkcheckDir+url
    command(cmd)

def tailorbirdSpellchecker(url, pwd):
    # function that creates the docker command for the Spellchecker tool.
    # the command is written into a text file that is then mounted into the image.
    aspellOptions = ""
    optiontxt = open('aspellOptions.txt', "w")
    for flag in data['tools']['spellchecker']['aspell']:
        for option in data['tools']['spellchecker']['aspell'][flag]:
            fullFlag = "--"+flag+"="+option+" "
            aspellOptions = aspellOptions+fullFlag
    print ("tailorbird has generated the options:")
    print (aspellOptions+"\n")
    optiontxt.write(aspellOptions)
    optiontxt.close()

    aspellOptionsDir = pwd+"/aspellOptions.txt"
    dictionariesDir = ""
    fileDir = ""
    for dictMount in data['tools']['spellchecker']['mount']['dictionaries']:
        scDir = pwd+"/"+dictMount+":/usr/local/lib/aspell-0.60/ "
        dictionariesDir = dictionariesDir+scDir
    for fileMount in data['tools']['spellchecker']['mount']['files']:
        scDir = pwd+"/"+fileMount+":/spellCheck "
        fileDir = fileDir+scDir

    cmd = "docker run "+aspellOptionsDir+":/aspellOptions.txt "+dictionariesDir+fileDir+url
    command(cmd)


if subprocess.call("which docker", shell=True) != 0:
    print ('Docker not found. Please install Docker at https://www.docker.com')
else: 
    print ('Docker found. Logging in...\n')
    # subprocess.call("docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY", shell=True)
    # does not automatically use password, manual input required

    with open('config.json') as config:
        data = json.load(config)
        
        for i in data['tools']:
            print ('checking for '+i+'...\n')
            # check if tool is activated

            if data['tools'][i]['active'] == 1:
                print(i+" is active")
                pwd = "-v \"$(pwd)\""               #current directory command in case of required mounting
                url = data['tools'][i]['url']       #docker image url
                cmd = "docker pull "+url            #docker pull command
                command(cmd)
                print ()

                if i == 'linkchecker':
                    tailorbirdLinkchecker(url, pwd)

                if i == 'spellchecker':
                    tailorbirdSpellchecker(url, pwd)

            else:
                print(i+" is deactivated")
            print()
    config.close()

cleanText()